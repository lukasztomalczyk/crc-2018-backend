﻿using System.Collections.Generic;
using CRC.Filters;
using Microsoft.AspNetCore.Mvc;
using CRC.Services.Abstract;
using CRC.Services.ViewModels;

namespace CRC.Controllers
{
    [Route("api/request/")]   
    public class RequestController : Controller
    {
        private readonly IRequestService _requestService;

        public RequestController(IRequestService requestService)
        {
            _requestService = requestService;
        }
      
        [HttpGet("getMyRequests/{id}")]
        public IEnumerable<ReadRequestViewModel> GetMyRequests(int id)
        {
            return _requestService.GetMyRequests(id);
        }       
      
        //todo dodaj filtr
        [HttpPost]
        public IActionResult CreateRequest([FromBody]CreateRequestViewModel request) 
        {           
            _requestService.CreateNewRequest(request);
            return Ok();
        }
        
        [HttpDelete("delete/{id}")]
        public IActionResult Delete(int id)
        {
            _requestService.Delete(id);
            return Ok();
        }
    }
}